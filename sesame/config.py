import os
import sys
import importlib
import logging

import __main__

##### Constants #####


##### Helpers #####

def scriptBasename():
    return os.path.splitext(os.path.basename(__main__.__file__))[0]

##### Settings #####

def initialiseConfig():
    # Default settings
    cfg = {
        'IFACE': 'wls3', # Local interface connected to the internet router
        'IGD2_XML': 'http://192.168.178.1:49000/igd2desc.xml', # URL of XML file with IGD2 description,
        'PROTOCOL': 'UDP', # Protocol for port mapping, must be either TCP or UDP
        'PORT_EXT': 51820, # Preferred external port to be requested. The actually reserved port may differ
        'PORT_INT': 51820, # The (internal) port to which the external port is mapped
        'PORT_LEASETIME': 0, # Number of seconds after which the port will be closed if lease is not renewd, 0 = infinite
        'IMAP_HOST': 'imap.example.com', # IMAP server host name or IP
        'SMTP_HOST': 'smtp.example.com', # SMTP server host name or IP
        'MAIL_USER': 'postman', # User to login to IMAP and SMTP servers
        'MAIL_PWD': 'secret', # Password to log into IMAP and SMTP servers
        'MAIL_SUBJECTS_OPEN': ('OPEN', 'Open Sesame!'), # Subjects to trigger opening the port(s)
        'MAIL_SUBJECTS_CLOSE': ('CLOSE', 'Close Sesame!'), # Subjects to trigger closing the port(s)
        'MAIL_ALLOWED_FROM': ('mail@example.org', 'me@another-example.org'), # Sender addresses that e-mails need to come from
        'MAIL_ALLOWED_TO': ('vpn@example.org'), # Receiver addresses that e-mails need to go to
        'MAIL_RESPONSE_FROM': 'vpn@example.org', # Sender's address for the trigger response e-mail
        'MAIL_RESPONSE_SUBJECT': "RE: %s", # Subject of the trigger response e-mail
        'MAIL_REPONSE_BODY': 'Hello World: %s!', # Body (text) of the trigger response e-mail. The placeholder represents the opend or closed port
        'DRY_MODE': True # If turned on, no changes will be made (via UPNP, IMAP) and no messages will be sent (via SMTP). Useful for testing. See log for corresponding messages (warnings).
    }
    # Load external settings, if present. Config file name is script file name with '_config' appended
    config_module_name = scriptBasename() + "_config"
    try:
        config_module = importlib.import_module(config_module_name)
        logging.debug("Loaded external configuration from " + config_module_name)
        cfg_ext = getattr(config_module, 'cfg')
        cfg.update(cfg_ext)
    except ImportError as error:
        print(error.args[0], file=sys.stderr)
        pass
    logging.debug("Configuration: " + str(cfg))
    return cfg
