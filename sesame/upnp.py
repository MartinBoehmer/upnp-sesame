import logging
import netifaces as ni
import asyncio

from async_upnp_client.client_factory import UpnpFactory
from async_upnp_client.aiohttp import AiohttpRequester

def getLocalIP(iface: str) -> str:
    ip = ni.ifaddresses(iface)[ni.AF_INET][0]['addr']
    logging.info("IP of local interface: %s", ip)
    return ip

async def createDevice(target: str):
    requester = AiohttpRequester()
    factory = UpnpFactory(requester)
    device = await factory.async_create_device(target)
    logging.debug("Created UPNP device: %s", device)
    return device

async def getInternetIP(device):
    service = device.find_service("urn:schemas-upnp-org:service:WANIPConnection:2")
    logging.debug("Found service: %s", service)
    action = service.action("GetExternalIPAddress")
    ipFuture = await action.async_call()
    ip = ipFuture['NewExternalIPAddress']
    logging.info("Internet IP: %s", ip)
    return ip

async def getOpenPorts(device, cfg):
    service = device.find_service("urn:schemas-upnp-org:service:WANIPConnection:2")
    logging.debug("Found service: %s", service)
    action = service.action("GetListOfPortMappings")
    portsFuture = await action.async_call(NewStartPort=1, NewEndPort=65535, NewProtocol=cfg['PROTOCOL'], NewManage=False, NewNumberOfPorts=100)
    ports = portsFuture['NewPortListing']
    logging.info("Open ports: %s", ports)
    return ports

async def addPortMapping(device, externalPort: int, proto: str, internalHost: str, internalPort: int, leaseDuration: int = 0, dryRun: bool = False):
    service = device.find_service("urn:schemas-upnp-org:service:WANIPConnection:2")
    logging.debug("Found service: %s", service)
    action = service.action("AddAnyPortMapping")
    if not dryRun:
        portsFuture = await action.async_call(NewRemoteHost='', NewExternalPort=externalPort, NewProtocol=proto, NewInternalPort=internalPort, NewInternalClient=internalHost, NewEnabled=True, NewPortMappingDescription='Beschreibung', NewLeaseDuration=leaseDuration)
        ports = portsFuture['NewReservedPort']
        logging.info("Added ports: %s", ports)
    else:
        ports = []
        logging.warning("DRY MODE: Not adding port %s mapping %s -> %s", proto, externalPort, internalPort)
    return ports
    
async def deletePortMapping(device, externalPort: int, proto: str, dryRun: bool = False):
    service = device.find_service("urn:schemas-upnp-org:service:WANIPConnection:2")
    logging.debug("Found service: %s", service)
    action = service.action("DeletePortMapping")
    if not dryRun:
        portsFuture = await action.async_call(NewRemoteHost='', NewExternalPort=externalPort, NewProtocol=proto)
        ports = portsFuture
        logging.info("Deleted ports: %s", externalPort)
    else:
        logging.warning("DRY MODE: Not deleting port mapping %s %s", externalPort, proto)
