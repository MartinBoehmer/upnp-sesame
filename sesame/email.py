from audioop import reverse
import logging
import asyncio
from typing import Optional
import aioimaplib
import datetime

from email.parser import BytesParser
from email.policy import default
from email.message import EmailMessage
import aiosmtplib
import ssl

from enum import Enum
from typing import Callable, Optional

RESPONSE_OK = 'OK'
HDR_MSG_ID = 'Message-ID'
HDR_SENT_DATE = 'Date'
HDR_SUBJECT = 'Subject'
IMAP_TIMEOUT = 30.0

async def connect(host: str, user: str, password: str, conn_lost_cb: Callable[[Optional[Exception]], None] = None) -> aioimaplib.IMAP4:
    ssl_ctx = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
    imap_client = aioimaplib.IMAP4(host=host, port=993, timeout=IMAP_TIMEOUT, conn_lost_cb=conn_lost_cb, ssl_context=ssl_ctx)
    await reconnect(imap_client, user, password)
    return imap_client

async def reconnect(imap_client: aioimaplib.IMAP4, user: str, password: str):
    await imap_client.wait_hello_from_server()
    logging.debug("Got Hello from %s", imap_client.host)
    await imap_client.login(user, password)
    logging.info("Logged into %s", imap_client.host)
    await imap_client.select()
    logging.debug("Selected INBOX")

async def disconnect(imap_client: aioimaplib.IMAP4) -> None:
    logging.debug("Attempt to logout while in state: %s", imap_client.get_state())
    await imap_client.logout()
    logging.info("Disconnected from %s", imap_client.host)

async def waitForNewMessage(imap_client: aioimaplib.IMAP4):
    logging.info("Listening for push events from IMAP server")
    idle = await imap_client.idle_start()
    while imap_client.has_pending_idle():
        pushMsg = await imap_client.wait_server_push()
        logging.debug("New message from server %s, type: %s", pushMsg, type(pushMsg))
        decodedMsg = ''
        if isinstance(pushMsg, list) and len(pushMsg) > 0:
            decodedMsg = pushMsg[0]
        if isinstance(decodedMsg, bytes):
            decodedMsg = decodedMsg.decode()
        newEmail = decodedMsg.__contains__("EXISTS")
        stopIdle = pushMsg == aioimaplib.STOP_WAIT_SERVER_PUSH
        logging.debug("Decoded message: %s, is new e-mail? %s, stop IDLE? %s", decodedMsg, newEmail, stopIdle)
        if newEmail or stopIdle:
            logging.info("Stop listening for push events from IMAP server")
            imap_client.idle_done()
            await asyncio.wait_for(idle, 1)
        
async def fetchUnseenMessages(imap_client: aioimaplib.IMAP4, dryMode: bool = False):
    messages = []
    response = await imap_client.uid_search("UNSEEN")
    if response.result == RESPONSE_OK:
        logging.debug("Search result: %s", response.lines)
        message_set = response.lines[0].decode().replace(" ", ",")
        if len(message_set) > 0:
            criteria = "(UID FLAGS BODY[HEADER])"
            #criteria = "(UID FLAGS BODYSTRUCTURE.PEEK BODY.PEEK[HEADER.FIELDS (From To Subject Message-ID Date)])"
            if dryMode:
                criteria = "(UID FLAGS BODY.PEEK[HEADER])"
                logging.warning("DRY MODE: Not setting the SEEN flag on processed e-mails")
            fetch_response = await imap_client.uid('fetch', message_set, criteria)
            if fetch_response.result == RESPONSE_OK:
                for i in range(1, len(fetch_response.lines), 3):
                    message = BytesParser(policy=default).parsebytes(fetch_response.lines[i])
                    messages.append(message)
                    logging.info("Fetched e-mail %s",  message.get('Message-ID'))
            else:
                logging.error(fetch_response)
    else:
        logging.error("Error searching messages: %s", response)
    return messages

async def filterMessages(messages, cfg):
    filteredMessages = []
    for message in messages:
        keep = True
        # Subject
        subject = message.get(HDR_SUBJECT)
        if not (subject in cfg['MAIL_SUBJECTS_OPEN'] or subject in cfg['MAIL_SUBJECTS_CLOSE']):
            keep = False
            logging.debug("Discarding e-mail %s due to invalid subject %s", message.get(HDR_MSG_ID), subject)
        # Sender
        senderEmail = message.get("From").addresses[0].addr_spec
        if not (senderEmail in cfg['MAIL_ALLOWED_FROM']):
            keep = False
            logging.debug("Discarding e-mail %s due to invalid sender address %s", message.get(HDR_MSG_ID), senderEmail)
        # Receiver
        receiverEmail = message.get("To").addresses[0].addr_spec
        if not (receiverEmail in cfg['MAIL_ALLOWED_TO']):
            keep = False
            logging.debug("Discarding e-mail %s due to invalid receiver address %s", message.get(HDR_MSG_ID), receiverEmail)
        # Body
        bodyText = message.get_body(('plain',))
        if not validateBody(bodyText):
            keep = False
            logging.debug("Discarding e-mail %s due to invalid body", message.get(HDR_MSG_ID))
        # Filter
        if (keep):
            logging.info("E-mail %s has passed all filter criteria",  message.get(HDR_MSG_ID))
            filteredMessages.append(message)
        else:
            logging.info("E-mail %s has not passed one or more filter criteria", message.get(HDR_MSG_ID))
    return filteredMessages

def validateBody(body: str) -> bool:
    return True

class SesameAction(Enum):
    NOOP = 'noop'
    OPEN = 'open'
    CLOSE = 'close'

def sortEmailsByDate(message):
    if HDR_SENT_DATE in message.keys():
        return message.get(HDR_SENT_DATE).datetime
    else:
        logging.warn("E-mail %s does not have a %s header, which is expected for sorting", message.get(HDR_MSG_ID), HDR_SENT_DATE)
        return datetime.max

async def deriveAction(messages, cfg):
    # Defaults
    action = SesameAction.NOOP
    actionMessage = None
    if len(messages) > 0:
        # Sort e-mails by date
        messages.sort(reverse=True, key=sortEmailsByDate)
        # Only consider last e-mail for action
        subject = messages[0].get(HDR_SUBJECT)
        if subject in cfg['MAIL_SUBJECTS_OPEN']:
            action = SesameAction.OPEN
            actionMessage = messages[0]
        elif subject in cfg['MAIL_SUBJECTS_CLOSE']:
            action = SesameAction.CLOSE
            actionMessage = messages[0]
    # Return
    if actionMessage is None: 
        logging.info("Dervied action %s", action)
    else:
        logging.info("Dervived action %s from e-mail %s", action, actionMessage.get(HDR_MSG_ID))
    return (action, actionMessage)

def buildResponse(triggerMessage: EmailMessage, openedOrClosedPort, cfg) -> EmailMessage:
    responseMessage = EmailMessage()
    responseMessage["From"] = cfg['MAIL_RESPONSE_FROM']
    responseMessage["To"] = triggerMessage.get('From')
    responseMessage["Subject"] = cfg['MAIL_RESPONSE_SUBJECT'] % triggerMessage.get(HDR_SUBJECT)
    responseMessage.set_content(cfg['MAIL_REPONSE_BODY'] % openedOrClosedPort)
    return responseMessage

async def sendResponse(message: EmailMessage, host: str, user: str, pwd: str, dryMode: bool = False):
    if not dryMode:
        await aiosmtplib.send(message, hostname=host, port=465, use_tls=True, username=user, password=pwd)
    else:
        logging.warning("DRY MODE: Not sending message %s", message)

