# "Open Sesame" for UPNP

Dynamically trigger the creation and deletion of port forwardings via UPNP, e. g. for temporary VPN access. Triggers come from e-mails.

How it works:
* The script subscribes to an IMAP account listening for new messages
* E-mails matching certain crtteria (e. g. sender, receiver, subject, body) trigger "open sesame" or "close sesame"
* If triggered, port forwardings on a UPNP-enabled router are dynamically created (open sesame) or deleted (close sesame)
* The opened / closed port is reported back by e-mail
* Once the port is open, you may connect to whatever service the port is designated for, e. g. VPN

**Disclaimer!** Do not rely on this script as your only security measure! Always harden the security of the machines and services you expose according to their respective documentation and state-of-the art best practices. This script does not do that. It can only open the door, but it cannot control, who enters or leaves through it.

## Prerequisites

The following libraries are required:

```
pip3 install netifaces async_upnp_client aioimaplib aiosmtplib
```

## Configuration

Adjust the configuration by copying or renaming ```sesame_config.py.example``` to ```sesame_config.py``` and adjust it according to your needs.

The logging configuration is located in ```[logging.conf]``` and may be adjusted as well.

## Usage

To run the script, just call

```python3 seasame.py```

## Auto-start

The script can be configured to automatically start on system boot. There are plenty of ways to achieve this. Here, we focus on using ```systemd```. For more details, see this [blog post](https://alexandra-zaharia.github.io/posts/stopping-python-systemd-service-cleanly/) and this [tutorial](https://code.luasoftware.com/tutorials/linux/auto-start-python-script-on-boot-systemd/).

Open the provided unit file ```upnp-sesame.service``` and check the paths and modify them to match your setup:

```
nano upnp-sesame.service
```

When done, create a symlink or copy the file to your systemd configuration and change the ownership:

```
sudo ln -sf upnp-sesame.service /etc/systemd/system/upnp-sesame.service
sudo chown root: upnp-sesame.service
```

Now, reload the system configuration, enable the service to start on boot and initially start the script:

```
sudo systemctl daemon-reload
sudo systemctl enable upnp-sesame.service
sudo systemctl start upnp-sesame.service
```

## License

Copyright Martin Böhmer

Licensed under the Apache License, Version 2.0 (the "License"); you may not use these files except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
