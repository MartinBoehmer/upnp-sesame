import logging
import logging.config

import asyncio
import signal

import sesame.config as sc
import sesame.upnp as su
import sesame.email as se

from typing import Callable, Optional

##### Logging #####

logging.config.fileConfig('logging.conf')

##### Signals #####

sigterm = False
cfg = None
imap_client = None

def signalTerminate():
    logging.info("Received SIGINT or SIGTERM")
    global sigterm
    sigterm = True
    global imap_client
    if not imap_client is None:
        asyncio.ensure_future(imap_client.stop_wait_server_push())

def signalHangUp():
    logging.info("Received SIGHUP")

def imapConnectionLost(ex: Optional[Exception]):
    global cfg
    global imap_client
    logging.info("Connection was lost. Exception: %s. Attempting to reconnect.", ex)
    if not imap_client is None:
        asyncio.ensure_future(imap_client.stop_wait_server_push())
        asyncio.ensure_future(se.reconnect(imap_client, cfg['MAIL_USER'], cfg['MAIL_PWD']))

##### Main #####

async def main():
    global sigterm
    global cfg
    global imap_client
    try:
        # Config
        cfg = sc.initialiseConfig()
        dryRun = cfg['DRY_MODE']
        imap_client = None
        # UPNP
        device = await su.createDevice(cfg['IGD2_XML'])
        localIP = su.getLocalIP(cfg['IFACE'])
        # E-mail
        imap_client = await se.connect(cfg['IMAP_HOST'], cfg['MAIL_USER'], cfg['MAIL_PWD'], imapConnectionLost)
        # Loop
        reservedPort = -1
        while not sigterm:
            # Check messages
            unseenMessages = await se.fetchUnseenMessages(imap_client, dryRun)
            filteredMEssages = await se.filterMessages(unseenMessages, cfg)
            action, triggerMessage = await se.deriveAction(filteredMEssages, cfg)
            # Anything to do?
            if action == se.SesameAction.OPEN:
                # Open
                reservedPort = await su.addPortMapping(device, cfg['PORT_EXT'], cfg['PROTOCOL'], localIP, cfg['PORT_INT'], cfg['PORT_LEASETIME'], dryRun)
                response = se.buildResponse(triggerMessage, reservedPort, cfg)
                await se.sendResponse(response, cfg['SMTP_HOST'], cfg['MAIL_USER'], cfg['MAIL_PWD'], dryRun)
            elif action == se.SesameAction.CLOSE:
                # Close
                if reservedPort <= 0:
                    logging.warning("No information about reserved/opened port found. Assuming external port from configuration: %s")
                    reservedPort = cfg['PORT_EXT']
                await su.deletePortMapping(device, reservedPort, cfg['PROTOCOL'], dryRun)
                response = se.buildResponse(triggerMessage, reservedPort, cfg)
                await se.sendResponse(response, cfg['SMTP_HOST'], cfg['MAIL_USER'], cfg['MAIL_PWD'], dryRun)
                reservedPort = -1
            # Wait for new messages
            await se.waitForNewMessage(imap_client)
    except RuntimeError as error:
        logging.error(error)
#    finally:
#        if not imap_client is None:
#            await se.disconnect(imap_client)

logging.info("Start")
loop = asyncio.get_event_loop()
loop.add_signal_handler(signal.SIGINT, signalTerminate)
loop.add_signal_handler(signal.SIGTERM, signalTerminate)
loop.add_signal_handler(signal.SIGHUP, signalHangUp)
loop.run_until_complete(main())
logging.info("Exit")
